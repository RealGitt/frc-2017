import wpilib


class Leds:

    def __init__(self):
        self.ds = wpilib.DriverStation.getInstance()

        self.outputs = [
            wpilib.DigitalOutput(0),
            wpilib.DigitalOutput(1)
        ]

    def execute(self, get_gear):
        alliance = 0
        try:
            alliance = self.ds.getAlliance()
        except:
            pass

        if get_gear:
            # set 3
            self.outputs[0].set(True)
            self.outputs[1].set(True)
        elif alliance == 0:
            # set 1
            self.outputs[0].set(True)
            self.outputs[1].set(False)
        else:
            # set 0
            self.outputs[0].set(False)
            self.outputs[1].set(False)
