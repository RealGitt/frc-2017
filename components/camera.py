import wpilib

from networktables.util import ntproperty


class Camera:

    aiming_light = wpilib.Relay

    goal_x = ntproperty('/vision/goal_x', 0)
    goal_y = ntproperty('/vision/goal_y', 0)
    selected_camera = ntproperty('/vision/selected_camera', 'lower')
    should_process = ntproperty('/vision/should_process', False)

    def show_low(self):
        self.selected_camera = 'lower'

    def show_high(self):
        self.selected_camera = 'upper'

    def start_processing(self):
        self.should_process = True

    def stop_processing(self):
        self.should_process = False

    def get_x(self):
        return self.goal_x * 0.8

    def execute(self):
        print(self.get_x())
        if self.should_process:
            self.aiming_light.set(2)
        else:
            self.aiming_light.set(0)
