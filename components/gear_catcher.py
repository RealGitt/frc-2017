import wpilib

from magicbot.magic_tunable import tunable
from common import ir as IR

ir = IR.IR

# Constants are variables that won't change.
# They belong here


class GearCatcher:

    has_gear = tunable(False)
    gear_sensor = ir

    def execute(self):
        if self.gear_sensor.get_inches() > 7:
            self.has_gear = False
        else:
            self.has_gear = True
