""" Dumper is used to dump the fuel tank """

import wpilib

from helpers import *
from magicbot.magic_tunable import tunable
from common import ir

# Constants are variables that won't change.
# They belong here


class Dumper:
    """
        The sole interaction between the robot and its dumping system
        occurs here.
    """

    dump_speed = tunable(1)
    reset_speed = tunable(0.8)

    position = tunable('default')

    dumper_motor = wpilib.Spark

    def __init__(self):
        pass

    # Info functions -- these give information to other methods

    # Verb functions -- these functions do NOT talk to motors directly. This
    # allows multiple callers in the loop to call our functions without
    # conflicts.

    def reset(self):
        self.position = 'default'

    def dump(self):
        self.position = 'dumping'

    # Execute function -- This is called automatically at the end of
    # teleopPeriodic / autonomousPeriodic. This directly interacts with
    # WPILib.

    def execute(self):
        """Actually makes the dumper move"""

        if self.position is 'dumping':
            self.dumper_motor.set(-self.dump_speed)
        else:
            self.dumper_motor.set(self.reset_speed)
