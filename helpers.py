""" These are helpers which are usable in other files """


def clamp(value, minn, maxn):
    """ Returns a value within a range that is closest to the given value """

    if value < minn:
        return minn
    elif value > maxn:
        return maxn
    else:
        return value


def square(value):
    if value < 0:
        return -(value ** 2)
    else:
        return value ** 2
