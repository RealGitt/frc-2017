import cv2
import math
import numpy as np

from cscore import CameraServer
from cscore.imagewriter import ImageWriter
from networktables import NetworkTables

image_writer = ImageWriter()

root_nt = NetworkTables.getTable('/')

nt = NetworkTables.getTable('vision')
nt.putString('selected_camera', 'lower')
nt.putBoolean('should_process', False)

HEIGHT = 240
WIDTH = 320

fps = 10
new_fps = 20

is_bright = False

# selected_camera = ntproperty('/vision/selected_camera', 'lower')
# should_process_frame = ntproperty('/vision/should_process', False)


def main():
    global is_bright

    cs = CameraServer.getInstance()
    cs.enableLogging()

    camera = cs.startAutomaticCapture(dev=1, name='upper_raw')
    camera2 = cs.startAutomaticCapture(dev=0, name='lower_raw')

    camera2.setResolution(WIDTH, HEIGHT)
    camera.setResolution(WIDTH, HEIGHT)

    # Get a CvSink. This will capture images from the camera
    highSink = cs.getVideo(name='upper_raw')
    lowSink = cs.getVideo(name='lower_raw')

    # (optional) Setup a CvSource. This will send images back to the Dashboard
    outputStream = cs.putVideo('processed', WIDTH, HEIGHT)

    # Allocating new images is very expensive, always try to preallocate
    img = np.zeros(shape=(240, 320, 3), dtype=np.uint8)

    while True:
        # Tell the CvSink to grab a frame from the camera and put it
        # in the source image.  If there is an error notify the output.
        if nt.getString('selected_camera') == 'lower':
            time, img = lowSink.grabFrame(img)
            selected_camera = camera2
        else:
            time, img = highSink.grabFrame(img)
            selected_camera = camera

        if time == 0:
            # Send the output the error.
            outputStream.notifyError(lowSink.getError())
            # skip the rest of the current iteration
            continue

        if nt.getBoolean('should_process'):
            new_fps = 10
            if is_bright:
                selected_camera.setBrightness(0)
                selected_camera.setExposureManual(10)
                is_bright = False
                # skip first time because of delay
                continue
        else:
            new_fps = 20
            if not is_bright:
                selected_camera.setBrightness(10)
                selected_camera.setExposureManual(50)
                is_bright = True

        if not new_fps == fps:
            selected_camera.setFPS(new_fps)

        if not nt.getBoolean('should_process'):
            outputStream.putFrame(img)
            continue

        hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

        # 80, 93, 64
        lower = np.array([70, 230, 40])
        upper = np.array([90, 255, 255])

        filtered = cv2.inRange(hsv, lower, upper)
        blurred = cv2.GaussianBlur(filtered, (7, 7), 0)

        # fill in gaps
        # kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
        # output = cv2.morphologyEx(filtered, cv2.MORPH_CLOSE, kernel,
        #                           iterations=10)
        output = blurred

        # image(output)

        im2, orig_contours, hierarchy = cv2.findContours(
            output, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

        # loop over the contours
        for c in orig_contours:
            cv2.drawContours(img, [c], -1, (255, 0, 255), 2)

        x = []
        y = []

        def find_center(contour):
            M = cv2.moments(contour)
            cX = int(M["m10"] / M["m00"])
            cY = int(M["m01"] / M["m00"])
            return cX, cY

        # contour_centers = map(find_center, orig_contours)
        # contours = orig_contours

        contour_areas = np.array(list(map(cv2.contourArea, orig_contours)))
        contours = [orig_contours[i] for i in np.argsort(contour_areas)[-2:]]

        # loop over the contours
        for c in contours:
            cX, cY = find_center(c)
            x.append(cX)
            y.append(cY)

            cv2.drawContours(img, [c], -1, (0, 0, 255), 2)

        half_width = WIDTH / 2

        if len(x) == 0 or len(y) == 0:
            outputStream.putFrame(img)
            side = root_nt.getString(
                '/autonomous/Side Lift (Vision)/side', 'left')
            print(side)
            if side == 'left':
                goal_x = -0.5
            else:
                goal_x = 0.5
            goal_y = 0

        else:
            img_x = int(np.mean(x))
            goal_x = (img_x - half_width) / half_width
            goal_y = int(np.mean(y))
            cv2.circle(img, (img_x, goal_y), 5, (0, 0, 255), -1)

        nt.putNumber('goal_x', goal_x)
        nt.putNumber('goal_y', goal_y)

        # Give the output stream a new image to display
        outputStream.putFrame(img)
        image_writer.setImage(img)
